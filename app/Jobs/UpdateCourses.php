<?php

namespace App\Jobs;

use App\Clients\CourseData\CoursesClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateCourses implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Dispatch jobs to update courses for the most recent terms.
     *
     * @param CoursesClient $coursesClient
     *   Client to interact with the UVic schedule listing API.
     */
    public function handle(CoursesClient $coursesClient) {
        $terms = $coursesClient->getRecentTerms();

        foreach ($terms as $term) {
            $this->dispatchUpdateJobsForTerm($coursesClient, $term);
        }
    }

    /**
     * For each subject in the given term, dispatch a job to fetch and update
     * courses for that subject.
     *
     * @param CoursesClient $coursesClient
     *   Client to interact with the UVic schedule listing API.
     * @param integer $term
     *   The term to dispatch course update jobs for.
     *   E.g. 202105 for the 2021 Summer term.
     */
    private function dispatchUpdateJobsForTerm(CoursesClient $coursesClient, int $term): void {
        $subjects = $coursesClient->getSubjects($term);

        foreach ($subjects as $subject) {
            UpdateCoursesForSubject::dispatch($term, $subject);
        }
    }
}
