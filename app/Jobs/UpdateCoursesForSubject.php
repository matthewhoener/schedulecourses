<?php

namespace App\Jobs;

use App\Clients\CourseData\Course as ClientCourse;
use App\Clients\CourseData\CoursesClient;
use App\Clients\CourseData\CoursesParseException;
use App\Clients\CourseData\MeetingTime as ClientMeetingTime;
use App\Models\Course;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class UpdateCoursesForSubject implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Term to update courses for, e.g. 202109 for 2021 Winter.
     *
     * @var string
     */
    private string $term;

    /**
     * Subject to update courses for, e.g. ENGR.
     *
     * @var string
     */
    private string $subject;

    /**
     * Create a new job instance.
     *
     * @param int $term
     *   Term to fetch course data for, e.g. 202109 for 2021 Winter.
     * @param string $subject
     *   Subject to fetch course data for, e.g. ENGR for Engineering.
     */
    public function __construct(int $term, string $subject) {
        $this->term = $term;
        $this->subject = $subject;
    }

    /**
     * Fetch and save course data for a given term and subject.
     *
     * @param CoursesClient $coursesClient
     *   Client to interact with the UVic schedule listing API.
     *
     * @throws CoursesParseException
     *   If there is a problem parsing the course data.
     */
    public function handle(CoursesClient $coursesClient) {
        // Fetch courses
        $courses = $coursesClient->getCoursesForSubject($this->term, $this->subject);

        // Store courses
        foreach ($courses as $course) {
            $this->saveCourseData($course, $this->term);
        }
    }

    /**
     * Map course and meeting times data to respective models and store to the
     * database, updating records if they already exist..
     *
     * @param ClientCourse $courseData
     *   Client to interact with the UVic schedule listing API.
     * @param int $term
     *   Term to associate the course with, e.g. 202109 for 2021 Winter.
     */
    protected function saveCourseData(ClientCourse $courseData, int $term) {
        $course = $this->updateOrCreateCourse($courseData, $term);

        $this->replaceMeetingTimes($course, $courseData->getMeetingTimes());
    }

    /**
     * Map the course data to the Course model and store in the database.
     *
     * If the CRN already exists, update instead of inserting a new record.
     *
     * @param ClientCourse $courseData
     *   Client to interact with the UVic schedule listing API.
     * @param int $term
     *   Term to associate the course with, e.g. 202109 for 2021 Winter.
     *
     * @return Course
     *   The persisted Course.
     */
    protected function updateOrCreateCourse(ClientCourse $courseData, int $term): Course {
        return Course::updateOrCreate(
            ['crn' => $courseData->getCrn()],
            ['term' => $term,
                'subject' => $courseData->getSubject(),
                'course' => $courseData->getCourseNum(),
                'name' => $courseData->getName(),
                'section' => $courseData->getSection(),
                'credits' => $courseData->getCredits(),
            ]);
    }

    /**
     * Delete any existing meeting time data for the course, then save the
     * new, up-to-date meeting times by mapping the client data to the
     * MeetingTime model and store in the database.
     *
     * @param Course $course
     *   The persisted course object.
     * @param Collection $meetingTimes
     *   Collection of ClientMeetingTime objects.
     */
    protected function replaceMeetingTimes(Course $course, Collection $meetingTimes): void {
        $course->meetingTimes()->delete();

        /** @var ClientMeetingTime $meetingTime */
        foreach ($meetingTimes as $meetingTime) {
            $course->meetingTimes()->create([
                'frequency' => $meetingTime->getFrequency(),
                'start_time' => $meetingTime->getStartTime(),
                'end_time' => $meetingTime->getEndTime(),
                'days' => $meetingTime->getDays(),
                'room' => $meetingTime->getRoom(),
                'startDate' => $meetingTime->getStartDate(),
                'endDate' => $meetingTime->getEndDate(),
                'class_type' => $meetingTime->getClassType(),
                'prof' => $meetingTime->getProf(),
            ]);
        }
    }
}
