<?php

namespace App\Http\Controllers;

use App\Jobs\UpdateCourses;

class CourseUpdateJobController extends Controller {
    public function index() {
        return view('course-update-jobs');
    }

    public function store() {
        UpdateCourses::dispatch();

        return redirect('course-update-jobs');
    }
}
