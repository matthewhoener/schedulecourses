<?php

namespace App\Http\Livewire;

use App\Jobs\UpdateCourses;
use App\Models\Job;
use Livewire\Component;

class CourseUpdater extends Component {
    public int $jobsRemaining;

    public function render() {
        $this->jobsRemaining = Job::all()->count();

        return view('livewire.course-updater');
    }

    public function updateCourses() {
        UpdateCourses::dispatch();

        $this->jobsRemaining = Job::all()->count();
    }
}
