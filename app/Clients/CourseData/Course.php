<?php


namespace App\Clients\CourseData;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class Course {
    protected string $name;
    protected int $crn;
    protected string $subject;
    protected string $course;
    protected string $section;
    protected string $credits;
    protected Collection $meetingTimes;

    /**
     * Create a Course object from a title string.
     *
     * Expects the title format to match the node text on the UVic course calendar.
     *
     * E.g. "Technical Communications: Written and Verbal - 21196 - ENGL 225 - A01"
     *
     * @param string $title The title string to parse information from.
     * @throws CoursesParseException If there is a problem parsing the title string.
     */
    public function __construct(string $title) {
        preg_match('/^(.*?) - (\d{5}) - ([A-Z0-9_-]{2,4}) (\d{3}[A-Z]?) - ([A-Z]\d{2})$/s', $title, $matches);

        if (count($matches) != 6) {
            Log::error('Could not parse course title: ' . $title);
            throw new CoursesParseException('Could not parse course title: ' . $title);
        }

        $this->setName($matches[1])
            ->setCrn($matches[2])
            ->setSubject($matches[3])
            ->setCourseNum($matches[4])
            ->setSection($matches[5]);

        $this->meetingTimes = new Collection();
    }

    /**
     * @return string The name of the course, e.g. 'Technical Communications: Written and Verbal'.
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name The name of the course to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    protected function setName(string $name): Course {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the 5-digit CRN number for the course, which uniquely identifies the class section for a given term.
     *
     * Note: CRNs at UVic are eventually reused in subsequent years.
     *
     * @return int The 5-digit CRN number for the course, e.g. '21196'
     */
    public function getCrn(): int {
        return $this->crn;
    }

    /**
     * @param string $crn The CRN to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    protected function setCrn(string $crn): Course {
        $this->crn = $crn;
        return $this;
    }

    /**
     * @return string The subject identifier for the course, e.g. 'ENGL'
     */
    public function getSubject(): string {
        return $this->subject;
    }

    /**
     * @param string $subject The subject to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    protected function setSubject(string $subject): Course {
        $this->subject = $subject;
        return $this;
    }

    /**
     * The course number which follows the subject identifier.
     *
     * These numbers are sometimes suffixed by a letter, for example in BIOL 150B
     *
     * @return string The course number, e.g. '150B'
     */
    public function getCourseNum(): string {
        return $this->course;
    }

    /**
     * @param string $course The course number to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    protected function setCourseNum(string $course): Course {
        $this->course = $course;
        return $this;
    }

    /**
     * @return string The course section, e.g. 'A01'
     */
    public function getSection(): string {
        return $this->section;
    }

    /**
     * @param string $section The course section to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    protected function setSection(string $section): Course {
        $this->section = $section;
        return $this;
    }

    /**
     * @return string The credits for the course, e.g. '1.500'
     */
    public function getCredits(): string {
        return $this->credits;
    }

    /**
     * @param string $credits The credits value to set.
     * @return Course The instance of Course that was updated, to allow chaining.
     * @throws CoursesParseException If there is a problem parsing the credits.
     */
    public function setCredits(string $credits): Course {
        if (1 != preg_match('/^\d{1,2}\.\d{3}$/', $credits)) {
            throw new CoursesParseException('Invalid credits: ' . $credits);
        }
        $this->credits = $credits;
        return $this;
    }

    /**
     * Get the meeting times for the class.
     *
     * There will usually be just one meeting time, but for some courses that don't meet every week,
     * such as Astronomy labs, or classes that meet in different locations or have a different prof on certain days,
     * there will be multiple meeting times for the course.
     *
     * Meeting times contain the time, days, location, date range, schedule type (lecture/lab/tutorial) and instructor.
     *
     * @return Collection The collection of MeetingTimes for the course.
     */
    public function getMeetingTimes(): Collection {
        return $this->meetingTimes;
    }

    /**
     * @param ?MeetingTime $meetingTime The MeetingTime to add to the course,
     *   or null if there is no meeting time, e.g. Master's Thesis or PhD Candidacy.
     * @return Course The instance of Course that was updated, to allow chaining.
     */
    public function addMeetingTime(?MeetingTime $meetingTime): Course {
        if ($meetingTime !== null) {
            $this->meetingTimes->add($meetingTime);
        }
        return $this;
    }


}
