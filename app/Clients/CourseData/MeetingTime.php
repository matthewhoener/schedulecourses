<?php


namespace App\Clients\CourseData;


use Carbon\Carbon;

class MeetingTime {
    public const DAY_SUNDAY = 64;
    public const DAY_MONDAY = 32;
    public const DAY_TUESDAY = 16;
    public const DAY_WEDNESDAY = 8;
    public const DAY_THURSDAY = 4;
    public const DAY_FRIDAY = 2;
    public const DAY_SATURDAY = 1;

    private string $frequency;
    private string $startTime;
    private string $endTime;
    private int $days;
    private string $room;
    private Carbon $startDate;
    private Carbon $endDate;
    private string $classType;
    private string $prof;

    /**
     * Create a MeetingTime for a course
     * @param string $frequency The frequency that the class meets, e.g. 'Every Week'.
     * @param string $times The times that the class meets, e.g. '8:30 am - 9:20 am' or 'TBA'.
     * @param string $days The days that the class meets, e.g. 'MRF', or empty.
     * @param string $room The building and room where the class meets, e.g. 'Cornett Building B135' or 'TBA'.
     * @param string $dateRange The date range the MeetingTime is active for, e.g. 'Jan 11, 2021 - Apr 07, 2021'.
     * @param string $classType The class type, e.g. 'Lecture', 'Lab', or 'Tutorial'.
     * @param string $prof The prof for the MeetingTime, e.g. 'Jason David Corless' or 'TBA'.
     * @throws CoursesParseException If there is a problem parsing the data.
     */
    public function __construct(
        string $frequency, string $times, string $days, string $room, string $dateRange, string $classType, string $prof) {

        $this->setFrequency($frequency)
            ->setTimes($times)
            ->setDays($days)
            ->setRoom($room)
            ->setDateRange($dateRange)
            ->setClassType($classType)
            ->setProf($prof);
    }

    /**
     * @return string The frequency for the MeetingTime. Usually 'Every Week'.
     */
    public function getFrequency(): string {
        return $this->frequency;
    }

    /**
     * @param string $frequency The MeetingTime frequency to set.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If the frequency is empty.
     */
    protected function setFrequency(string $frequency): MeetingTime {
        if (empty($frequency)) {
            throw new CoursesParseException('Frequency is empty');
        }
        $this->frequency = $frequency;
        return $this;
    }

    /**
     * @return string The start time for the MeetingTime in 24h format, e.g. '9:00' or 'TBA'.
     */
    public function getStartTime(): string {
        return $this->startTime;
    }

    /**
     * @return string The end time for the MeetingTime in 24h format, e.g. '13:00' or 'TBA'.
     */
    public function getEndTime(): string {
        return $this->endTime;
    }

    /**
     * @param string $times The times string to parse to set the start and end times from, e.g. '8:30 am - 9:20 am' or 'TBA'
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If there is a problem parsing the times.
     */
    protected function setTimes(string $times): MeetingTime {
        $times = CoursesParser::parseTimes($times);

        $this->startTime = $times['start'];
        $this->endTime = $times['end'];
        return $this;
    }

    /**
     * This method returns a number that represents the days on which the class meets.
     *
     * Each day is represented by a different power of 2 so the days that the class meets can be described by 7 bits.
     *
     * The DAY constants can be checked against this value with a logical OR to see if the class meets on that day.
     *
     * @return int The sum of day flags for the MeetingTime that indicate on which days the class meets.
     */
    public function getDays(): int {
        return $this->days;
    }

    /**
     * Sets the days integer value as the sum of DAY flags that the provided days string represents.
     *
     * @param string $days The string representation of the days to set, e.g. 'MR' for Monday and Thursday, or blank.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If there is a problem parsing the days string.
     */
    protected function setDays(string $days): MeetingTime {
        if (1 != preg_match('/^(M?)(T?)(W?)(R?)(F?)(S?)(U?)$/', $days, $matches)) {
            if ($days == ' ') {
                // Days are TBA or there are no set days
                $this->days = 0;
                return $this;
            }
            throw new CoursesParseException('Could not parse days: ' . $days);
        }

        $dayFlags = 0;
        $dayFlags |= ($matches[1]) ? self::DAY_MONDAY : 0;
        $dayFlags |= ($matches[2]) ? self::DAY_TUESDAY : 0;
        $dayFlags |= ($matches[3]) ? self::DAY_WEDNESDAY : 0;
        $dayFlags |= ($matches[4]) ? self::DAY_THURSDAY : 0;
        $dayFlags |= ($matches[5]) ? self::DAY_FRIDAY : 0;
        $dayFlags |= ($matches[6]) ? self::DAY_SATURDAY : 0;
        $dayFlags |= ($matches[7]) ? self::DAY_SUNDAY : 0;

        $this->days = $dayFlags;
        return $this;
    }

    /**
     * @return string The room for the MeetingTime, e.g. 'Cornett Building B135' or 'TBA'.
     */
    public function getRoom(): string {
        return $this->room;
    }

    /**
     * @param string $room The room to set for the MeetingTime.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException if the provided room string is empty.
     */
    protected function setRoom(string $room): MeetingTime {
        if (empty($room)) {
            throw new CoursesParseException('Room is empty');
        }

        $this->room = $room;
        return $this;
    }

    /**
     * @return Carbon The start date for the MeetingTime.
     */
    public function getStartDate(): Carbon {
        return $this->startDate;
    }

    /**
     * @return Carbon The end date for the MeetingTime.
     */
    public function getEndDate(): Carbon {
        return $this->endDate;
    }

    /**
     * @param string $dateRange The date range to set for the MeetingTime, e.g. 'Jan 06, 2020 - Apr 03, 2020'.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If there is a problem parsing the date range.
     */
    protected function setDateRange(string $dateRange): MeetingTime {
        $dateRange = CoursesParser::parseDates($dateRange);
        $this->startDate = $dateRange['start'];
        $this->endDate = $dateRange['end'];
        return $this;
    }

    /**
     * @return string The class type, e.g. 'Lecture', 'Lab', or 'Tutorial'.
     */
    public function getClassType(): string {
        return $this->classType;
    }

    /**
     * @param string $classType The class type to set for the MeetingTime.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If the class type is empty.
     */
    protected function setClassType(string $classType): MeetingTime {
        if (empty($classType)) {
            throw new CoursesParseException('Class type is empty');
        }

        $this->classType = $classType;
        return $this;
    }

    /**
     * @return string The prof for the MeetingTime, e.g. 'Jason David Corless' or 'TBA'.
     */
    public function getProf(): string {
        return $this->prof;
    }

    /**
     * @param string $prof The prof to set for the MeetingTime.
     * @return MeetingTime The instance of MeetingTime that was updated, to allow chaining.
     * @throws CoursesParseException If the prof is empty.
     */
    protected function setProf(string $prof): MeetingTime {
        if (empty($prof)) {
            throw new CoursesParseException('Prof is empty');
        }

        $this->prof = $prof;
        return $this;
    }


}
