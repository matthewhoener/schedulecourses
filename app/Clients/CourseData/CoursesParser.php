<?php


namespace App\Clients\CourseData;


use Carbon\Carbon;
use Exception;

class CoursesParser {
    /**
     * Parses a time string consisting of two 12h time formats separated by a dash and returns a two-element array
     * containing the start and end times in 24h time format.
     *
     * @param string $timesString The string to parse, e.g. '9:00 am - 1:00 pm'.
     * @return array An array containing the start and end times, e.g. ['start' => '9:00', 'end' => '13:00'].
     * @throws CoursesParseException If the time cannot be parsed.
     */
    public static function parseTimes(string $timesString): array {
        if($timesString == 'TBA') {
            return [
                'start' => 'TBA',
                'end' => 'TBA'
            ];
        }

        $times = explode(' - ', $timesString);

        if (count($times) != 2) {
            throw new CoursesParseException('Could not parse time: ' . $timesString);
        }

        $start = strtotime($times[0]);
        $end = strtotime($times[1]);

        if ($start === false || $end === false || $end <= $start) {
            throw new CoursesParseException('Could not parse time: ' . $timesString);
        }

        return [
            'start' => date('H:i', strtotime($times[0])),
            'end' => date('H:i', strtotime($times[1])),
        ];
    }

    /**
     * Parses days information, returning the sum of day flags for the days that the class occurs on.
     *
     * @param string $days The days listed of the class, e.g. "TWF" for Tuesday, Wednesday, Friday
     * @return int
     */
    public static function parseDays(string $days): int {
        $dayFlags = 0;

        $dayFlags |= (preg_match('/U/', $days)) ? MeetingTime::DAY_SUNDAY : 0;
        $dayFlags |= (preg_match('/M/', $days)) ? MeetingTime::DAY_MONDAY : 0;
        $dayFlags |= (preg_match('/T(?!h)/', $days)) ? MeetingTime::DAY_TUESDAY : 0;
        $dayFlags |= (preg_match('/W/', $days)) ? MeetingTime::DAY_WEDNESDAY : 0;
        $dayFlags |= (preg_match('/(R|Th)/', $days)) ? MeetingTime::DAY_THURSDAY : 0;
        $dayFlags |= (preg_match('/F/', $days)) ? MeetingTime::DAY_FRIDAY : 0;
        $dayFlags |= (preg_match('/A/', $days)) ? MeetingTime::DAY_SATURDAY : 0;

        return $dayFlags;
    }

    /**
     * Parses a date range for a course, returning the start and end dates as Carbon instances.
     *
     * @param string $dateString The date range for the course, e.g. 'Jan 06, 2020 - Apr 03, 2020'
     * @return array An array containing Carbon instances for the start and end date
     * @throws CoursesParseException If the date cannot be parsed.
     */
    public static function parseDates(string $dateString): array {
        $dates = explode('-', $dateString);

        if (count($dates) != 2) {
            throw new CoursesParseException('Could not parse dates: ' . $dateString);
        }

        try {
            $start = new Carbon($dates[0]);
            $end = new Carbon($dates[1]);
        } catch (Exception $e) {
            throw new CoursesParseException('Could not parse dates: ' . $dateString . ' - ' . $e->getMessage());
        }

        if ($start->gt($end)) {
            throw new CoursesParseException('Could not parse dates - start date is after end date: ' . $dateString);
        }

        return compact('start', 'end');
    }
}
