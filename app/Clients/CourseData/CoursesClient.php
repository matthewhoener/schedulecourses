<?php

namespace App\Clients\CourseData;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

class CoursesClient {
    // This page has a select list with all the terms that can be searched
    private const URL_CLASS_SCHEDULE_SEARCH = 'https://www.uvic.ca/BAN1P/bwckschd.p_disp_dyn_sched';

    // This page has a select list containing the subjects available for a given term
    private const URL_TERM_SEARCH = 'https://www.uvic.ca/BAN1P/bwckgens.p_proc_term_date';

    // This page shows course data for a given term and subject
    private const URL_TERM_SUBJECT_SEARCH = 'https://www.uvic.ca/BAN1P/bwckschd.p_get_crse_unsec';

    // List of terms to get courses for
    private const NUMBER_OF_TERMS_TO_FETCH = 3;

    /**
     * Get the three most recent terms.
     *
     * @return array The three most recent terms available.
     */
    public function getRecentTerms(): array {
        $response = Http::get(self::URL_CLASS_SCHEDULE_SEARCH);
        $crawler = new Crawler($response->body());

        $terms = $crawler->filter('#term_input_id > option')->each(function($node) {
            return $node->attr('value');
        });

        return array_slice($terms, 1, self::NUMBER_OF_TERMS_TO_FETCH);
    }

    /**
     * Get the subjects available for a given term.
     *
     * @param string $term The term to fetch subjects for, in the format yyyymm where mm should be 01, 05, or 09.
     * @return array The list of subjects available for the given term.
     */
    public function getSubjects(string $term): array {
        $responseBody = $this->postRequest(self::URL_TERM_SEARCH, [
            'p_calling_proc' => 'bwckschd.p_disp_dyn_sched',
            'p_term' => $term
        ]);
        $crawler = new Crawler($responseBody);

        return $crawler->filter('#subj_id > option')->each(function($node) {
            return $node->attr('value');
        });
    }

    /**
     * Get the course data for a given term and subject.
     *
     * @param int $term The term to get course data for, e.g. 202009 for the 2020 Winter term starting in September.
     * @param string $subject The subject to get course data for, e.g. SENG for Software Engineering.
     * @return Collection A collection of course parsed course data.
     * @throws CoursesParseException If there is a problem parsing  the course data.
     */
    public function getCoursesForSubject(int $term, string $subject): Collection {
        // This is ridiculous, but it's the way UVic does it
        $responseBody = $this->postRequestOverride(self::URL_TERM_SUBJECT_SEARCH,
            "term_in=${term}&sel_subj=dummy&sel_day=dummy&sel_schd=dummy&sel_insm=dummy&sel_camp=dummy"
            . "&sel_levl=dummy&sel_sess=dummy&sel_instr=dummy&sel_ptrm=dummy&sel_attr=dummy"
            . "&sel_subj=${subject}&sel_crse=&sel_title=&sel_schd=%25&sel_insm=%25&sel_from_cred=&sel_to_cred="
            . "&sel_camp=%25&sel_levl=%25&sel_ptrm=%25&sel_instr=%25&begin_hh=0&begin_mi=0&begin_ap=a&end_hh=0"
            . "&end_mi=0&end_ap=a");

        return $this->scrapeCoursesForSubject($responseBody);
    }

    /**
     * Scrape data from the Class Schedule Listing page html and return the relevant pieces in an array.
     *
     * @param string $responseBody The Class Schedule Listing page html.
     * @return Collection A collection of Course objects.
     * @throws CoursesParseException If there is a problem parsing the course data.
     */
    public function scrapeCoursesForSubject(string $responseBody): Collection {
        // The first table.datadisplaytable contains all course data
        /** @var Crawler $crawler */
        $crawler = (new Crawler($responseBody))->filter('table.datadisplaytable')->eq(0);

        // Every pair of <tr> rows contains the course title, then the meeting times
        $scrapedData = $crawler->children('tr')->each(function(Crawler $node, $i) {
            if ($i % 2 == 0) {
                return $node->filter('a')->text();
            }

            $data['credits'] = $this->scrapeCredits($node);
            $data['meetingTimes'] = $this->scrapeMeetingTimes($node->filter('table.datadisplaytable'));

            return $data;
        });

        $courses = collect();
        foreach ($scrapedData as $i => $element) {
            if ($i % 2 == 0) {
                $course = new Course($element);
                continue;
            }

            $course->setCredits($element['credits']);
            foreach ($element['meetingTimes'] as $meetingTime) {
                $course->addMeetingTime($meetingTime);
            }
            $courses->add($course);
        }

        return $courses;
    }

    /**
     * Scrape the credits data from the node.
     *
     * @param Crawler $node The tr containing containing the credits information.
     * @return string The credits value for the course, e.g. "1.500"
     * @throws CoursesParseException
     */
    private function scrapeCredits(Crawler $node): string {
        // UVic's data is horribly formatted and the credit information is not contained by an element. It's sitting
        // around a bunch of other elements and <br> tags. This makes it difficult to scrape and requires a regex to
        // extract it.
        if (1 != preg_match('/(\d+.\d{3}) Credits/', $node->text(), $matches) || 2 != count($matches)) {
            throw new CoursesParseException('Could not parse credits: ' . $node->text());
        }
        return $matches[1];
    }

    /**
     * Scrape meeting times for a course.
     *
     * @param Crawler $meetingTimesTableNode The Scheduled Meeting Times table.
     * @return MeetingTime[] Array of MeetingTime Objects.
     * @throws CoursesParseException If there is a problem parsing the meeting times.
     */
    private function scrapeMeetingTimes(Crawler $meetingTimesTableNode): array {
        if ($meetingTimesTableNode->count() == 0) {
            return [];
        }

        $meetingTimes = $meetingTimesTableNode->children('tr')->each(function(Crawler $meetingTimesRowNode, $i) {
            if ($i == 0) {
                // First row contains headings
                return null;
            }
            return $this->scrapeMeetingTime($meetingTimesRowNode->children());
        });

        // First element is null because the first <tr> contains headings
        return array_slice($meetingTimes, 1);
    }

    /**
     * Scrape the meeting times data from the Scheduled Meeting Times table row.
     *
     * @param Crawler $node The tr node containing the meeting time td cells.
     * @return ?MeetingTime An object containing the frequency, time, days, room, date range, class type, and prof,
     *   or null if there are no meeting times, e.g. for Master's Thesis or PhD Candidacy.
     * @throws CoursesParseException If there is a problem parsing the data.
     */
    private function scrapeMeetingTime(Crawler $node): ?MeetingTime {
        if ($node->count() < 7) {
            return null;
        }
        return new MeetingTime(
        // E.g. Every Week
            $node->eq(0)->text(),

            // E.g. 10:00 am - 11:20 am
            $node->eq(1)->text(),

            // E.g. MTh
            $node->eq(2)->text(),

            // E.g. David Strong Building C103
            $node->eq(3)->text(),

            // E.g. Jan 06, 2020 - Apr 03, 2020
            $node->eq(4)->text(),

            // E.g. Lecture
            $node->eq(5)->text(),

            // E.g. Michael J Zastre (P)
            $node->eq(6)->text(),
        );
    }

    /**
     * Post a request with the provided data to the specified url.
     *
     * @param string $url The address to post the request to.
     * @param array $data The data to include in the body of the request.
     * @return string The body of the response received.
     */
    private function postRequest(string $url, array $data = []): string {
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ),
        );
        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

    /**
     * This function is required to be able to post malformed data to the class search endpoint in the format that
     * UVic's server expects it to be in.
     *
     * This is how the UVic public API does it ¯\_(ツ)_/¯
     *
     * @param string $url The address to post the request to.
     * @param string $data The data to post in the body of the request, already encoded as a string.
     * @return string The body of the response received.
     */
    private function postRequestOverride(string $url, string $data): string {
        $options = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => $data,
            ),
        );
        $context = stream_context_create($options);
        return file_get_contents($url, false, $context);
    }

}
