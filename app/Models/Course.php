<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course extends Model {
    use HasFactory;

    protected $fillable = [
        'term',
        'crn',
        'subject',
        'course',
        'name',
        'section',
        'credits',
    ];

    /**
     * Get the meeting times for the course.
     */
    public function meetingTimes(): HasMany {
        return $this->hasMany(MeetingTime::class);
    }
}
