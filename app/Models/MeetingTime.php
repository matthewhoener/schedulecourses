<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MeetingTime extends Model {
    use HasFactory;

    protected $fillable = [
        'frequency',
        'start_time',
        'end_time',
        'days',
        'room',
        'startDate',
        'endDate',
        'class_type',
        'prof',
    ];

    /**
     * Get the course that owns this meeting time.
     */
    public function course(): BelongsTo {
        return $this->belongsTo(Course::class);
    }
}
