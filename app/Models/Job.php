<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {
    /**
     * The payload column stores json-encoded data.
     *
     * This allows the data inside to be easily accessed.
     *
     * @var string[]
     */
    protected $casts = [
        'payload' => 'array',
    ];

    /**
     * @return mixed The displayName stored in the payload column.
     */
    public function name() {
        return $this->payload['displayName'];
    }
}
