<?php

namespace Tests\Client\CourseData;

use App\Clients\CourseData\CoursesClient;
use App\Clients\CourseData\MeetingTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Tests\TestCase;

class CoursesClientTest extends TestCase {

    public function test_it_can_fetch_three_most_recent_terms() {
        $client = new CoursesClient();
        $terms = $client->getRecentTerms();

        // Should have 3 terms: spring, summer, fall
        $this->assertCount(3, $terms);

        // Term format is yyyymm where mm is the starting month for the term
        // 01 - January for Spring; 05 - May for Summer; 09 - September for Winter
        $this->assertEquals(1, preg_match('/^\d{4}0[159]$/', $terms[0]));
        $this->assertEquals(1, preg_match('/^\d{4}0[159]$/', $terms[1]));
        $this->assertEquals(1, preg_match('/^\d{4}0[159]$/', $terms[2]));
    }

    public function test_it_can_fetch_a_list_of_subjects_for_a_term() {
        $client = new CoursesClient();
        $subjects = $client->getSubjects($this->lastSpringTerm());

        $this->assertTrue(count($subjects) > 50);

        foreach ($subjects as $subject) {
            $this->assertEquals(1, preg_match('/^[A-Z-]{2,4}$/', $subject));
        }
    }

    public function test_it_can_scrape_course_data_from_the_class_schedule_listing() {
        $responseBody = File::get(storage_path('framework/testing/samples/class-schedule-listing.html'));
        $client = new CoursesClient();
        $courseData = $client->scrapeCoursesForSubject($responseBody);

        $allWeekDays = MeetingTime::DAY_MONDAY
            | MeetingTime::DAY_TUESDAY
            | MeetingTime::DAY_WEDNESDAY
            | MeetingTime::DAY_THURSDAY
            | MeetingTime::DAY_FRIDAY;

        self::assertCount(6, $courseData);

        // Check first course from sample doc
        self::assertEquals("An Introduction to Contemporary Art Photography", $courseData[0]->getName());
        self::assertEquals("31576", $courseData->first()->getCRN());
        self::assertEquals("ART", $courseData->first()->getSubject());
        self::assertEquals("141", $courseData->first()->getCourseNum());
        self::assertEquals("A01", $courseData->first()->getSection());
        self::assertEquals("1.500", $courseData->first()->getCredits());
        self::assertEquals("Every Week", $courseData->first()->getMeetingTimes()[0]->getFrequency());
        self::assertEquals("16:00", $courseData->first()->getMeetingTimes()[0]->getStartTime());
        self::assertEquals("18:20", $courseData->first()->getMeetingTimes()[0]->getEndTime());
        self::assertEquals($allWeekDays, $courseData->first()->getMeetingTimes()[0]->getDays());
        self::assertEquals("TBA", $courseData->first()->getMeetingTimes()[0]->getRoom());
        self::assertEquals(new Carbon("Aug 04, 2020"), $courseData->first()->getMeetingTimes()[0]->getStartDate());
        self::assertEquals(new Carbon("Aug 21, 2020"), $courseData->first()->getMeetingTimes()[0]->getEndDate());
        self::assertEquals("Lecture", $courseData->first()->getMeetingTimes()[0]->getClassType());
        self::assertEquals("Yang Liu (P)", $courseData->first()->getMeetingTimes()[0]->getProf());

        // Check last course from sample doc
        self::assertEquals("MFA Degree Exhibition", $courseData->last()->getName());
        self::assertEquals("30039", $courseData->last()->getCRN());
        self::assertEquals("ART", $courseData->last()->getSubject());
        self::assertEquals("598", $courseData->last()->getCourseNum());
        self::assertEquals("A02", $courseData->last()->getSection());
        self::assertEquals("0.000", $courseData->last()->getCredits());
        self::assertEquals("Every Week", $courseData->last()->getMeetingTimes()[0]->getFrequency());
        self::assertEquals("TBA", $courseData->last()->getMeetingTimes()[0]->getStartTime());
        self::assertEquals("TBA", $courseData->last()->getMeetingTimes()[0]->getEndTime());
        self::assertEquals(0, $courseData->last()->getMeetingTimes()[0]->getDays());
        self::assertEquals("TBA", $courseData->last()->getMeetingTimes()[0]->getRoom());
        self::assertEquals(new Carbon("May 04, 2020"), $courseData->last()->getMeetingTimes()[0]->getStartDate());
        self::assertEquals(new Carbon("Jul 31, 2020"), $courseData->last()->getMeetingTimes()[0]->getEndDate());
        self::assertEquals("Lecture", $courseData->last()->getMeetingTimes()[0]->getClassType());
        self::assertEquals("TBA", $courseData->last()->getMeetingTimes()[0]->getProf());
    }

    public function test_it_can_fetch_course_data_for_a_term_and_subject() {
        $client = new CoursesClient();
        $term = $this->lastSpringTerm();

        $coursesData = $client->getCoursesForSubject($term, 'CSC');

        self::assertTrue(count($coursesData) >= 10);

        $assertedMeetingTimes = false;
        foreach ($coursesData as $courseData) {
            self::assertNotEmpty($courseData->getName());
            self::assertNotEmpty($courseData->getCRN());
            self::assertNotEmpty($courseData->getSubject());
            self::assertNotEmpty($courseData->getCourseNum());
            self::assertNotEmpty($courseData->getSection());
            self::assertNotEmpty($courseData->getCredits());
            foreach ($courseData->getMeetingTimes() as $meetingTime) {
                // Some courses don't have meeting times, like for a thesis.
                // Need to make sure these were successfully checked at least once though.
                $assertedMeetingTimes = true;
                self::assertNotEmpty($meetingTime->getFrequency());
                self::assertNotEmpty($meetingTime->getStartTime());
                self::assertNotEmpty($meetingTime->getEndTime());
                self::assertNotNull($meetingTime->getDays());
                self::assertNotEmpty($meetingTime->getRoom());
                self::assertNotEmpty($meetingTime->getStartDate());
                self::assertNotEmpty($meetingTime->getEndDate());
                self::assertNotEmpty($meetingTime->getClassType());
                self::assertNotEmpty($meetingTime->getProf());
            }
        }

        self::assertTrue($assertedMeetingTimes);
    }

    public function test_it_can_can_parse_multiple_meeting_times_on_a_course() {
        $responseBody = File::get(storage_path('framework/testing/samples/class-schedule-listing.anth.html'));
        $client = new CoursesClient();
        $courses = $client->scrapeCoursesForSubject($responseBody);

        self::assertTrue(count($courses) > 0);
    }

    private function lastSpringTerm(): string {
        return (now()->year - 1) . '01';
    }
}
