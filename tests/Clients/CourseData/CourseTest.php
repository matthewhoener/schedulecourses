<?php

namespace Tests\Clients\CourseData;

use App\Clients\CourseData\Course;
use App\Clients\CourseData\CoursesParseException;
use Tests\TestCase;

class CourseTest extends TestCase {

    public function test_it_can_parse_titles_with_a_dash_in_the_subject() {
        $course = new Course('Learning Strategies for University Success - 31945 - ED-D 101 - A01');

        self::assertEquals('Learning Strategies for University Success', $course->getName());
        self::assertEquals('31945', $course->getCrn());
        self::assertEquals('ED-D', $course->getSubject());
        self::assertEquals('101', $course->getCourseNum());
        self::assertEquals('A01', $course->getSection());
    }

    public function test_it_can_set_and_get_credits() {
        $course = new Course('Technical Communications: Written and Verbal - 21196 - ENGL 225 - A01');
        $course->setCredits('1.500');

        self::assertEquals('1.500', $course->getCredits());
    }

    public function test_it_throws_exception_if_title_cannot_be_parsed() {
        $this->expectExceptionObject(new CoursesParseException('Could not parse course title: invalid title string'));

        new Course('invalid title string');
    }

    public function test_it_throws_exception_if_credits_cannot_be_parsed() {
        $this->expectExceptionObject(new CoursesParseException('Invalid credits: invalid credits string'));

        $course = new Course('Technical Communications: Written and Verbal - 21196 - ENGL 225 - A01');
        $course->setCredits('invalid credits string');
    }
}
