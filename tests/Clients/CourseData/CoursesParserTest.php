<?php

namespace Tests\Client\CourseData;

use App\Clients\CourseData\CoursesParseException;
use App\Clients\CourseData\CoursesParser;
use App\Clients\CourseData\MeetingTime;
use Carbon\Carbon;
use Tests\TestCase;

class CoursesParserTest extends TestCase {

    public function test_it_can_parse_times() {
        $times = CoursesParser::parseTimes('10:00 am - 2:00pm');

        self::assertEquals('10:00', $times['start']);
        self::assertEquals('14:00', $times['end']);

    }

    public function test_it_can_parse_TBA_times() {
        $times = CoursesParser::parseTimes('TBA');

        self::assertEquals('TBA', $times['start']);
        self::assertEquals('TBA', $times['end']);

    }

    public function test_it_throws_an_exception_if_times_is_not_a_range() {
        $this->expectException(CoursesParseException::class);

        CoursesParser::parseTimes('no dash present');
    }

    public function test_it_throws_an_exception_if_times_range_is_invalid() {
        $this->expectException(CoursesParseException::class);

        CoursesParser::parseTimes('morning - evening');
    }

    public function test_it_can_parse_days() {
        $days = CoursesParser::parseDays('&nbsp;');
        self::assertEquals(0, $days);

        $days = CoursesParser::parseDays('U');
        self::assertEquals(MeetingTime::DAY_SUNDAY, $days);

        $days = CoursesParser::parseDays('M');
        self::assertEquals(MeetingTime::DAY_MONDAY, $days);

        $days = CoursesParser::parseDays('T');
        self::assertEquals(MeetingTime::DAY_TUESDAY, $days);

        $days = CoursesParser::parseDays('W');
        self::assertEquals(MeetingTime::DAY_WEDNESDAY, $days);

        $days = CoursesParser::parseDays('R');
        self::assertEquals(MeetingTime::DAY_THURSDAY, $days);

        $days = CoursesParser::parseDays('Th');
        self::assertEquals(MeetingTime::DAY_THURSDAY, $days);

        $days = CoursesParser::parseDays('F');
        self::assertEquals(MeetingTime::DAY_FRIDAY, $days);

        $days = CoursesParser::parseDays('A');
        self::assertEquals(MeetingTime::DAY_SATURDAY, $days);

        $days = CoursesParser::parseDays('TR');
        self::assertEquals(
            MeetingTime::DAY_TUESDAY
            | MeetingTime::DAY_THURSDAY,
            $days
        );

        $days = CoursesParser::parseDays('TWF');
        self::assertEquals(
            MeetingTime::DAY_TUESDAY
            | MeetingTime::DAY_WEDNESDAY
            | MeetingTime::DAY_FRIDAY,
            $days
        );

        $days = CoursesParser::parseDays('UMTWRFA');
        self::assertEquals(
            MeetingTime::DAY_SUNDAY
            | MeetingTime::DAY_MONDAY
            | MeetingTime::DAY_TUESDAY
            | MeetingTime::DAY_WEDNESDAY
            | MeetingTime::DAY_THURSDAY
            | MeetingTime::DAY_FRIDAY
            | MeetingTime::DAY_SATURDAY,
            $days
        );
    }

    public function test_it_can_parse_dates() {
        $dates = CoursesParser::parseDates('Jan 06, 2020 - Apr 03, 2020');
        self::assertEquals((new Carbon('2020-01-06'))->toDateTimeString(), $dates['start']);
        self::assertEquals((new Carbon('2020-04-03'))->toDateTimeString(), $dates['end']);

    }
}
