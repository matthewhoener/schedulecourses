<?php

namespace Tests\Jobs;

use App\Clients\CourseData\Course as ClientCourse;
use App\Clients\CourseData\CoursesClient;
use App\Clients\CourseData\MeetingTime as ClientMeetingtime;
use App\Jobs\UpdateCoursesForSubject;
use App\Models\Course;
use App\Models\MeetingTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateCoursesForSubjectTest extends TestCase {
    use RefreshDatabase;

    public function test_it_saves_fetched_course_data() {
        $courseData = $this->mockClientCourse(
            'Technical Communications: Written and Verbal - 21196 - ENGL 225 - A01'
        );
        $mockCoursesClient = $this->createMock(CoursesClient::class);
        $mockCoursesClient->method('getCoursesForSubject')
            ->willReturn(collect([$courseData]));

        $job = new UpdateCoursesForSubject(202101, 'ENGL');
        $job->handle($mockCoursesClient);

        $this->assertDatabaseCount('courses', 1);
        $this->assertDatabaseCount('meeting_times', 1);
    }

    public function test_it_updates_course_and_meeting_times_if_crn_already_exists() {
        // Setup
        Course::factory()
            ->has(MeetingTime::factory()->count(2))
            ->create(['crn' => '21196']);
        $courseData = $this->mockClientCourse(
            'Technical Communications: Written and Verbal - 21196 - ENGL 225 - A01'
        );
        $mockCoursesClient = $this->createMock(CoursesClient::class);
        $mockCoursesClient->method('getCoursesForSubject')
            ->willReturn(collect([$courseData]));

        // Assert state before course update
        $this->assertDatabaseCount('courses', 1);
        $this->assertDatabaseCount('meeting_times', 2);

        // Course update
        $job = new UpdateCoursesForSubject(202101, 'ENGL');
        $job->handle($mockCoursesClient);

        // Assert state after course update
        $this->assertDatabaseCount('courses', 1);
        $this->assertEquals('Technical Communications: Written and Verbal', Course::first()->name);
        $this->assertDatabaseCount('meeting_times', 1);
    }

    private function mockClientCourse(string $courseTitle): ClientCourse {
        $meetingTime = new ClientMeetingtime(
            'Every Week',
            'TBA',
            'MWF',
            'TBA',
            'Jan 11, 2021 - Apr 07, 2021',
            'Lecture',
            'TBA'
        );

        return (new ClientCourse($courseTitle))
            ->setCredits('1.000')
            ->addMeetingTime($meetingTime);
    }
}
