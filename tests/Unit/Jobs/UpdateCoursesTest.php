<?php

namespace Tests\Jobs;

use App\Clients\CourseData\CoursesClient;
use App\Jobs\UpdateCoursesForSubject;
use App\Jobs\UpdateCourses;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class UpdateCoursesTest extends TestCase {

    public function test_it_dispatches_update_course_jobs_for_each_term(){
        Queue::fake();

        $mockCoursesClient = $this->createMock(CoursesClient::class);
        $mockCoursesClient->method('getRecentTerms')
            ->willReturn([202101, 202105, 202109]);
        $mockCoursesClient->method('getSubjects')
            ->withConsecutive([202101], [202105], [202109])
            ->willReturnOnConsecutiveCalls(['CSC'], ['ENGR'], ['ENGL']);

        $job = new UpdateCourses();
        $job->handle($mockCoursesClient);

        Queue::assertPushed(UpdateCoursesForSubject::class, 3);
    }
}
