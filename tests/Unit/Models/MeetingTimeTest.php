<?php

namespace Tests\Models;

use App\Models\Course;
use App\Models\MeetingTime;
use Tests\TestCase;

class MeetingTimeTest extends TestCase {

    public function test_it_belongs_to_a_course() {
        $meetingTime = MeetingTime::factory()->create();

        self::assertInstanceOf(Course::class, $meetingTime->course);
    }
}
