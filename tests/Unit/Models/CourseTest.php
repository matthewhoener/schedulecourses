<?php

namespace Tests\Models;

use App\Models\Course;
use App\Models\MeetingTime;
use Tests\TestCase;

class CourseTest extends TestCase {

    public function test_it_has_many_meeting_times() {
        $course = Course::factory()
            ->has(MeetingTime::factory()->count(2))
            ->create();

        self::assertInstanceOf(Course::class, $course);
        self::assertCount(2, $course->meetingTimes);
        self::assertContainsOnlyInstancesOf(MeetingTime::class, $course->meetingTimes);
    }
}
