<?php

namespace Tests\Feature;

use App\Jobs\UpdateCourses;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class CourseUpdateJobTest extends TestCase {
    use RefreshDatabase;
    
    public function test_it_shows_a_course_update_button() {
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)
            ->get(route('course-update-jobs.index'));

        $response->assertSee('Update Courses');
    }

    public function test_a_course_update_job_can_be_dispatched() {
        Queue::fake();
        $admin = User::factory()->create();

        $response = $this->actingAs($admin)
            ->post(route('course-update-jobs.store'));

        $response->assertRedirect(route('course-update-jobs.index'));
        Queue::assertPushed(UpdateCourses::class);
    }
}
