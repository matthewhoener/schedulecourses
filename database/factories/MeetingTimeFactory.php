<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\MeetingTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class MeetingTimeFactory extends Factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MeetingTime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array {
        $startTime = $this->startTime();
        $dateRange = $this->dateRange();

        return [
            'course_id' => Course::factory(),
            'frequency' => 'Every Week',
            'start_time' => $startTime->format('h:i a'),
            'end_time' => $this->endTime($startTime)->format('h:i a'),
            'days' => $this->faker->numberBetween(1, 127),
            'room' => $this->faker->regexify('[A-Z]{3,4} \d{3}'),
            'startDate' => $dateRange['start'],
            'endDate' => $dateRange['end'],
            'class_type' => $this->classType(),
            'prof' => $this->faker->name,
        ];
    }

    protected function startTime(): Carbon {
        $time = Carbon::now();

        $time->hour = $this->faker->numberBetween(8, 18);
        $time->minute = $this->faker->boolean ? 0 : 30;

        return $time;
    }

    protected function endTime(Carbon $time): Carbon {
        $time->hour += $this->faker->numberBetween(1, 3);
        $time->minute = $this->faker->boolean ? 0 : 30;

        return $time;
    }

    protected function dateRange(): array {
        $termsDates = [
            ['start' => 'Jan 11', 'end' => 'Apr 12'],   // Spring
            ['start' => 'May 05', 'end' => 'Jul 30'],   // Summer
            ['start' => 'Sep 09', 'end' => 'Dec 04'],   // Winter
        ];

        $dateRanger = $termsDates[$this->faker->numberBetween(0, 2)];

        return [
            'start' => new Carbon($dateRanger['start']),
            'end' => new Carbon($dateRanger['end']),
        ];
    }

    protected function classType(): string {
        $classTypes = ['Lecture', 'Lab', 'Tutorial'];

        return $classTypes[$this->faker->numberBetween(0, 2)];
    }
}
