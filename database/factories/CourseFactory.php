<?php

namespace Database\Factories;

use App\Models\Course;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory {
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Course::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array {
        return [
            'name' => $this->faker->sentence(),
            'term' => $this->faker->regexify('202\d0[159]'),
            'crn' => $this->faker->randomNumber(5, true),
            'subject' => $this->faker->regexify('[A-Z]{3,4}'),
            'course' => $this->faker->regexify('[0-9]{3}[A-D]?'),
            'section' => $this->faker->regexify('[ABT][01][0-9]'),
            'credits' => '1.000'
        ];
    }
}
