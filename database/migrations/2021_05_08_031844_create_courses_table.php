<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('courses', function(Blueprint $table) {
            $table->id();
            $table->unsignedInteger('term');
            $table->unsignedInteger('crn')->unique();
            $table->string('subject');
            $table->string('course');
            $table->string('name');
            $table->string('section');
            $table->string('credits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('courses');
    }
}
