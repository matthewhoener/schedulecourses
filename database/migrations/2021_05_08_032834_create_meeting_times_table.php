<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingTimesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('meeting_times', function(Blueprint $table) {
            $table->id();
            $table->foreignId('course_id');
            $table->string('frequency');
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('days');
            $table->string('room');
            $table->timestamp('startDate');
            $table->timestamp('endDate');
            $table->string('class_type');
            $table->string('prof');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('meeting_times');
    }
}
