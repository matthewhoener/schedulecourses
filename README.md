# ScheduleCourses

## Installation
```bash
# Clone Repository
git clone git@gitlab.com:ionsquare/schedulecourses.git
cd schedulecourses

# Copy env file
cp .env.example .env

# Initial composer install
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs

# Start schedulecourses
vendor/bin/sail up

# Generate app key
vendor/bin/sail artisan key:generate

# Install javascript dependencies
vendor/bin/sail npm install

# Create Test Database
docker exec -it schedulecourses_mysql_1 mysql -uroot -p'warning this password is under version control' \
  -e 'create database laravel_test; grant all on laravel_test.* to sail'

# Migrate DB
vendor/bin/sail artisan migrate
```

## Starting and stopping
```bash
# Start application
vendor/bin/sail up

# Stop application
vendor/bin/sail down
```

## Development
```bash
# Compile assets
vendor/bin/sail npm run dev

# Run tests
vendor/bin/sail artisan test
```

