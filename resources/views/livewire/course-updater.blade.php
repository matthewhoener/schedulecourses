<div @if ($jobsRemaining > 0) wire:poll @endif>
    <p>Course Update</p>

    <button wire:click="updateCourses" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Update Courses</button>

    <p>Number of jobs remaining: {{ $jobsRemaining }}</p>
</div>
