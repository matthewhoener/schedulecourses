<?php

use App\Http\Controllers\CourseUpdateJobController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Authenticated routes
Route::middleware(['auth:sanctum'])->group(function(){
   Route::resource('course-update-jobs', CourseUpdateJobController::class)->only([
      'index', 'store'
   ]);
});
